<?php
session_start();
include_once('login_checker.php');


$stud_obj = $db->query("SELECT * FROM `students` WHERE `student_id` = '$uid'");
$stud_no = $stud_obj->num_rows;
extract($_GET);
if($stud_no==0){
	
	?>
		<table width="600" class="content_table" border="0" cellpadding="10" cellspacing="1">
		 <tr>
			<Td align="center"><b style="color:#F00">Invalid Student ID</b></Td>
		  </tr>
		</table>
    <?php
	
}
else{
	$stud_details = $stud_obj->row;
	$student_id = $stud_details['student_id'];
	
	$today = date('Y-m-d');
	$week = date('Y-m-d',strtotime("+7 days"));
	?>


<br />


			
	<?php
	$sql = "SELECT s.sob_id, s.sob, s.level_id, l.level, s.topic_id, t.topic, s.expected_completion_date,  s.expected_start_date, s.url FROM `sobs` s, `levels` l, `topics` t WHERE s.level_id = l.level_id and s.topic_id = t.topic_id";
	
	if($levels!=""){
		$sql.=" AND l.level_id IN ($levels)";
	}
	
	if($topics!=""){
		$sql.=" AND t.topic_id IN ($topics)";
	}
	
	if($from_date!=""){
		$from_date = date_mysql($from_date);
		$sql.=" AND s.expected_completion_date >= '$from_date' ";
	}
	
	if($to_date!=""){
		$to_date = date_mysql($to_date);
		$sql.=" AND s.expected_completion_date <= '$to_date' ";
	}
	
	if($sob_status!="" && $sob_status!="1,2"){
		if($sob_status=="1"){
			$sql.=" AND s.sob_id IN (SELECT sob_id FROM `sob_observations` WHERE `student_id` = '$student_id') ";
		}
		else if($sob_status=="2"){
			$sql.=" AND s.sob_id NOT IN (SELECT sob_id FROM `sob_observations` WHERE `student_id` = '$student_id')";
		}
	}
	
	if($sob_ids!=""){
		$sql.=" AND s.sob_id IN ($sob_ids) ";
	}
	if($keywords_sob_id!=""){
		$sql.=" AND s.sob_id IN ($keywords_sob_id) ";
	}
	
	$sql.=' ORDER BY s.level_id, s.topic_id ';
	
	$sobs_obj = $db->query($sql);
	$sobs_no = $sobs_obj->num_rows;
	
	$divnames=1;
	
	$total = 0;
	$expected = 0;
	$observed = 0;
	
	if($sobs_no!=0){
		$sobs = $sobs_obj->rows;
		
		$topic_name = "";
		$level_name = "";
		
		$level_arr = array();
		
		?>
        
        <table width="600" cellpadding="0" cellspacing="0">
	
				
				<tr>
                  <td align="left"><strong>ECD</strong> - <strong>E</strong>xpected <strong>C</strong>ompletion <strong>D</strong>ate</td>
				  <td align="right">
                  <ul class="observe_legend">
                  	<li class="color_box sob_expired">&nbsp;</li>
                    <li>Overdue</li>
                    <li class="color_box sob_observed">&nbsp;</li>
                    <li>Observed</li>
                    <li class="color_box sob_expire_today">&nbsp;</li>
                    <li>Active</li>
                  </ul>
                  </td>
				</tr>
	
			</table>
        
		<table width="600" border="0" cellpadding="10" cellspacing="1">
		<?php
		$s=0;
		foreach($sobs as $sob){
		
		$sob_id = $sob['sob_id'];
		
		if($level_name!=$sob['level']){
	
		
			
			
			?>
            
           
            
			<tr>
				<td class="level_name" colspan="2"><?php echo $sob['level'];?></td>
			</tr>
             <tr>
            	<td colspan="2" id="progress_bar_display_area">
                
                <div class="process_content_bar" id="<?php echo $divnames++;?>">
                <?php
				$achieved = ($observed/$total)*100;
				$achievement = ($expected/$total)*100;
				?>
                <div class="process_bar">
            	<div class="achieved_bar" style="width:<?php echo $achieved;?>%;"></div>
                <div class="expected_achievement" style="width:<?php echo $achievement;?>%;"></div>
                <div class="level_legend">
                	<table width="330" border="0" cellspacing="0" cellpadding="0" align="right">
                      <tr>
                        <td bgcolor="#C8C7C7" width="10">&nbsp;</td>
                        <td>&nbsp;<?php echo 'Total : <b>' . $total .'</b>';?>&nbsp;&nbsp;</td>
                        <td bgcolor="#008800" width="10">&nbsp;</td>
                        <td>&nbsp;<?php echo 'Observed :  <b>' . $observed.'</b>';?>&nbsp;&nbsp;</td>
                        <td bgcolor="#000000" width="1">&nbsp;</td>
                        <td>&nbsp;<?php echo 'Expected by '.date_ft($today).' : <b>' . $expected.'</b>';?>&nbsp;&nbsp;</td>
                      </tr>
                    </table>

                </div>
            </div>
                
                </div>
                
                </td>
            </tr>
			<?php
			$total = 0;
	$expected = 0;
	$observed = 0;
			$level_name=$sob['level'];
		}
		
		if($topic_name!=$sob['topic']){
			?>
			<tr>
				<td class="topic_name" colspan="2"><?php echo $sob['topic'];?></td>
			</tr>
			<?php
			$topic_name=$sob['topic'];
			$s=0;
		}
		
		$s++;
		$total++;
		
		if($sob['expected_completion_date']<$today){
			$expected++;
		}
		
		$obs_obj = $db->query("SELECT * FROM `sob_observations` WHERE `student_id` = '$student_id' AND `sob_id` = '$sob_id'");
		$obs_no = $obs_obj->num_rows;
		$obs_row = $obs_obj->row;
		
		$notes_obj = $db->query("SELECT * FROM `sob_notes` WHERE `student_id` = '$student_id' AND `sob_id` = '$sob_id'");
		$notes_no = $notes_obj->num_rows;
		?>
		<tr class="sob_highlight">
			  <td align="left" <?php if($sob['expected_completion_date']<$today && $obs_no==0)  echo 'class="sob_expired"';  else if($sob['expected_start_date']<$today && $sob['expected_completion_date']>$today && $obs_no==0) echo 'class="sob_expire_today"'; else if($obs_row['observed_on']!='0000-00-00' && $obs_row['observed_on']!='') echo 'class="sob_observed"'; ?> width="10"><?php echo $sob['sob_id'];?></td>
			  <td align="left">
			  <?php echo $sob['sob'];?>
              <?php
			  if($sob['url']!=""){
				  echo '<br /><strong>URL : </strong>';
				  $urls = explode(' ',$sob['url']);
				  foreach($urls as $url){
					  if($url!="" && $url!=" "){
					  	echo '<a href="'.$url.'" target="_blank">'.$url.'</a> &nbsp; &nbsp; ';
					  }
				  }
			  }
			  	$sob_keywords = "";
			    $keywords_obj = $db->query("SELECT t.tag_id, k.keyword FROM `keywords` k, `keywords_sobs` t WHERE t.keyword_id = k.keyword_id and t.sob_id = '$sob_id'");
				$key_no = $keywords_obj->num_rows;
				if($key_no>0){
					$keywords = $keywords_obj->rows;
					
					foreach($keywords as $keyword){
						$sob_keywords.='<div class="keyword_tags" style="margin-bottom:5px;">'.$keyword['keyword']."</div>";
					}
				}
				//$sob_keywords= substr($sob_keywords, 0, -2);
			  ?>
              </Td>
		</tr>
			<tr>      
			  <td colspan="2">
			  <div style="float:left; width:400px;">
              
              <strong>ECD:</strong> <?php echo date_ft($sob['expected_completion_date']); if($sob['expected_completion_date']<$week && $obs_no==0 && $sob['expected_completion_date']>=$today) { echo ' <b>'. DayDifference($today, $sob['expected_completion_date']) . ' day(s) left</b>'; }?> | 
              
              <strong>START DATE :</strong>  <?php echo date_ft($sob['expected_start_date']);?>
              
              <br />
              <strong style="float:left; margin-right:5px; margin-top:5px;">KEYWORDS :</strong> <?php echo $sob_keywords;?>
              
              </div> 
              
              <div style="float:right;"><a class="small green button" id="notes_btn_<?php echo $sob_id;?>" onClick="sob_notes('<?php echo $sob_id;?>')" href="javascript:;">Comments (<?php echo $notes_no;?>)</a><?php /*?>&nbsp;&nbsp;<a class="small green button" onClick="sob_discussion('<?php echo $sob_id;?>')" href="javascript:;">Notes</a><?php */?></div>
              <?php
			  if($obs_no==0){
					echo '<div style="float:right;padding-top:5px;">Not yet observed &nbsp;&nbsp;</div>';
				}
				else{
				$staffid = $obs_row['observed_by'];
				$obs_id = $obs_row['observation_id'];
				
					if($staffid!="0"){
						$teacher_obj = $db->query("SELECT * FROM `staffs` WHERE `staff_id` = '$staffid'");
						$teacher_details = $teacher_obj->row;
						$teacher_name = $teacher_details['firstname'] . " " . $teacher_details['lastname'];
						$observed_by = ' by ' . $teacher_name;
						
					}
					else{
						$observed_by = '';
					}
				$observed++;
				
					
				
					if($obs_row['observed_on']!='0000-00-00'){
					 echo '<div style="float:right;padding-top:5px;"><strong>Observed on</strong> : ' . date_ft($obs_row['observed_on']). $observed_by . '  &nbsp;&nbsp;</div>';
					}
					else{
					
					}
				
				}
				?>
			  			
              
			  </td>
			</tr>
		<?php
		}
		?>
        <tr>
        	<td colspan="2"><div class="process_content_bar" id="<?php echo $divnames++;?>">
             <?php
				$achieved = ($observed/$total)*100;
				$achievement = ($expected/$total)*100;
				?>
                <div class="process_bar">
            	<div class="achieved_bar" style="width:<?php echo $achieved;?>%;"></div>
                <div class="expected_achievement" style="width:<?php echo $achievement;?>%;"></div>
                <div class="level_legend">
                	<table width="330" border="0" cellspacing="0" cellpadding="0" align="right">
                      <tr>
                        <td bgcolor="#C8C7C7" width="10">&nbsp;</td>
                        <td>&nbsp;<?php echo 'Total : ' . $total;?>&nbsp;&nbsp;</td>
                        <td bgcolor="#008800" width="10">&nbsp;</td>
                        <td>&nbsp;<?php echo 'Observed : ' . $observed;?>&nbsp;&nbsp;</td>
                        <td bgcolor="#000000" width="1">&nbsp;</td>
                        <td>&nbsp;<?php echo 'Expected by '.date_ft($today).' : ' . $expected;?>&nbsp;&nbsp;</td>
                      </tr>
                    </table>

                </div>
            </div>
            </div></td>
        </tr>
	</table>
    <script>
	for(var i=1;i<=<?php echo $divnames;?>;i++){
		var j = parseInt(i)+1
		$('#'+i).html($('#'+j).html());
	}
	</script>

		<?php
	}
	else{
		?>
		<table width="600" class="content_table" border="0" cellpadding="10" cellspacing="1" align="left">
		 <tr>
			<Td align="center"><b>-- No results found --</b></Td>
		  </tr>
		</table>
		<?php
	}
}
?>

