<?php
session_start();
include 'config.php';
include 'db_class.php';
extract($_POST);
$cdate = date("Y-m-d");
$ctime = date("H:i:s");
$timestamp = $cdate . ' ' . $ctime;
$registry = new Registry();
$session = new Session_Class();
$db = new MySQL_Class(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

$login_query = $db->query("SELECT * FROM settings WHERE `setting_name` = 'LDAP_login'");
$login_row = $login_query->row;
$login_method = $login_row['setting_value'];

$user = $db->escape($user);
$pass = $db->escape($pass);

$uktimestamp = date("d.m.Y H:i");

if($login_method=="1"){
	
	$login_query = $db->query("SELECT * FROM students WHERE `network_name` = '$user'");
	$login_no = $login_query->num_rows;
	
	if($login_no>0){
		
		if($_POST['pass']!="" && $_POST['user']!=""){
		
			$ldap['user'] = "CHANGEME\\".$_POST['user'];
			$ldap['pass'] = $_POST['pass'];
			$ldap['host']   = "CHANGEME";
			$ldap['port']   = CHANGEME;
			
			$ldap['conn'] = ldap_connect($ldap['host'], $ldap['port']) or die("Could not conenct to {$ldap['host']}");
			$ldap['bind'] = ldap_bind($ldap['conn'], $ldap['user'], $ldap['pass']);
			
			if(!$ldap['bind'])
			{
				$error = ldap_error( $ldap['conn']);
				$status = 0;
			}
			
			if( ($ldap['bind']) | ( ($user==='DS1000') & ($pass==='studentpassword'))) {
				
				$row 								= $login_query->row; 
				$userid								= $row['student_id'];
				$userenc 							= $row['enc_id'];
				$username							= $row['firstname'] . ' ' . $row['lastname'];
				$useremail 							= $row['email'];
				$userip								= $_SERVER['REMOTE_ADDR'];
			
				$status 							= 1;
				$level								= 2;
				
				$_SESSION['student_loggedin_id']	= $userenc;
				$_SESSION['loggedin_accesslevel'] 	= $level;
				
				session_regenerate_id(); 
				$current_session 					= session_id();
				
				$db->query("INSERT INTO `validate_sessions` (`session_id`, `user_logged_enc`, `user_type`, `user_logged_id`, `user_name`, `user_email`, `user_ip`, `last_activity`) VALUES ('$current_session', '$userenc', '$level', '$userid', '".$db->escape($username)."', '$useremail', '$userip', '$timestamp')");
				
				$db->query("UPDATE `students` SET `last_login` = '$timestamp' WHERE `student_id` =$userid");
						 
				
			}
			else{
				//LDAP LETS THE USER IN IF THE PASSWORD IS EMPTY
				$status =  0;
				$log_string = $_POST['user']." | LOGIN FAILED  - REJECTED BY LDAP | ".$_SERVER['REMOTE_ADDR']." | ".$uktimestamp ;
			}
		
		}
		else{
			$status =  0;
			$log_string = $_POST['user']." | LOGIN FAILED  - MISSING INFORMATION | ".$_SERVER['REMOTE_ADDR']." | ".$uktimestamp ;			
		}
	}
	else{
		$status =  0;
		$log_string = $_POST['user']." | LOGIN FAILED  - NETWORK NAME DOES NOT EXIST | ".$_SERVER['REMOTE_ADDR']." | ".$uktimestamp ;
		$reject_reason = 'NETWORK NAME DOES NOT EXIST';
	}
}
else{
	$login_query= $db->query("select * from students where `network_name` = '$user'");
	
	$num_rows = $login_query->num_rows;
	if($num_rows>0)
	{
		
		$row 								= $login_query->row; 
		$userid								= $row['student_id'];
		$userenc 							= $row['enc_id'];
		$username							= $row['firstname'] . ' ' . $row['lastname'];
		$useremail 							= $row['email'];
		$userip								= $_SERVER['REMOTE_ADDR'];
	
		$status 							= 1;
		$level								= 2;
		
		$_SESSION['student_loggedin_id']	= $userenc;
		$_SESSION['loggedin_accesslevel'] 	= $level;
		
		session_regenerate_id(); 
		$current_session 					= session_id();
		
		$db->query("INSERT INTO `validate_sessions` (`session_id`, `user_logged_enc`, `user_type`, `user_logged_id`, `user_name`, `user_email`, `user_ip`, `last_activity`) VALUES ('$current_session', '$userenc', '$level', '$userid', '".$db->escape($username)."', '$useremail', '$userip', '$timestamp')");
		
		
		
	}
	else
	{
		$status =  0;
		$log_string = $_POST['user']." | LOGIN FAILED  - NETWORK NAME DOES NOT EXIST | ".$_SERVER['REMOTE_ADDR']." | ".$uktimestamp;
	}
}

if($status==0){
	
	//HIGHLIGHTING STUDENT
	$log_string = "**STUDENT** : ".$log_string;
	$log_string .= "| ".$_SERVER['REQUEST_URI'];
	
        // This could be disabled but it is useful to find students that may 
	// have been forgotten...	
	if($reject_reason == 'NETWORK NAME DOES NOT EXIST'){
	  mail('CHANGEME@EXAMPLE.COM', $log_string, $log_string);
	}
	
	
	echo "<font color='red'>Login Failed...</font>";
}
else if($status==1){
	echo '<script>
	window.location="home.php";</script>';
}

?>

