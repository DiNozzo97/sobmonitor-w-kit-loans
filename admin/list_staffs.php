<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('login_checker.php');
extract($_GET);


?>

 <table width="100%" class="content_table" border="0" cellpadding="10" cellspacing="1">
	  <tr class="table_heading">
          <th width="25" align="left">S.No</th>
          <th align="left">First Name</th>
          <th align="left">Last Name</th>
          <th width="150" align="left">Email</th>
          <th width="150" align="left">Network Name / User ID</th>
          <?php
		$capability_obj = $db->query("SELECT * FROM `capabilities` WHERE 1 ");
		$capabilities = $capability_obj->rows;
		
		$cap_arr = array();
		
		foreach($capabilities as $capability){
			
			array_push($cap_arr, $capability['capability_id']);
			?>
          	<th width="120" align="left"><?php echo $capability['capability_description'];?></th>
          	<?php
		}
		?>
          <th width="50" align="left">Action</th>
      </tr>
<?php
$query="select * from `staffs` where 1";





$staff_obj = $db->query($query);
$staffs_no = $staff_obj->num_rows;

if($staffs_no!=0){
	$staffs = $staff_obj->rows;
	$s=0;
	foreach($staffs as $staff){
	$s++;
	$staffid = $staff['staff_id'];
	?>
    <tr>
          <td align="left" valign="top"><?php echo $s;?></td>
          <td valign="top" align="left"><?php echo $staff['firstname'];?></td>
          <td valign="top" align="left"> <?php echo $staff['lastname'];?></Td>
          <td align="left" valign="top"><?php echo $staff['email'];?></Td>
          <td align="left" valign="top"><?php echo $staff['network_name'];?></Td>
			<?php
			foreach($cap_arr as $cap){
				
				$chk_cap = $db->query("SELECT * FROM `capability_mapping` WHERE `staff_id` = '$staffid' AND `capability_id` = '$cap'");
				$chk = $chk_cap->num_rows;
          		?>
                <td id="capability_access_<?php echo $staffid;?>_<?php echo $cap;?>">
                	<?php
					if($chk==0){
						echo '<img src="images/nocapability.png" />';
					}
					else{
						echo '<img src="images/tick.png" />';
					}
					?>
                </td>
				<?php
			}
			?>
          <td align="left" valign="top" id="edit_button_<?php echo $staff['staff_id'];?>">
       
          <a href="javascript:;" onclick="edit_staff('<?php echo $staff['staff_id'];?>')" title="Edit Staff Capabilities"><img src="images/edit.png" border="0" /></a>
          </td>
   </tr>
    <?php
	}
	?>
    </table>
    <?php
}
else{
	?>
     <tr>
        <Td align="center" colspan="7"><br /><b>-- No results found --</b></Td>
        </tr>
    <?php
}
?>
