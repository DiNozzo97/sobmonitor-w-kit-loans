<?php
session_start();
include_once 'login_checker.php';
if(has_capabilities($uid, 'Attendance')==false){
	header('Location:home.php');
	exit();
}

include 'header.php';

include('weeks.php');

$crn = array();
$crn_obj = $db->query("SELECT  * FROM `CRNlist` WHERE 1");
$crn_details = $crn_obj->rows;

foreach($crn_details as $crn_detail){
	$crn[$crn_detail['crn']] = $crn_detail['codetype'] . " - " . $crn_detail['room'] . " - " . $crn_detail['day'] . " (" . $crn_detail['starttime'] . " - " . $crn_detail['endtime'] . ")";
}

?>

<div id="wrapper"> 
    <div id="wrapper_content"> 
        <h1 class="page_title">Non-registered students</h1>



		<?php
		$query = $db->query("SELECT  studid, crn, week FROM `attendance` WHERE studid NOT IN (SELECT student_number FROM `students` WHERE 1)");
		?>
        		<table width="100%" border="0" cellspacing="1" cellpadding="10" class="content_table">
                <tr class="table_heading">
                <th width="50">S.No</th>
                <th width="150">Student Number</th>
                <th>Attended CRNs</th>
                </tr>
                <?php
				if($query->num_rows>0){
				$non_registered = $query->rows;
				$i=0;
				foreach($non_registered as $students){
					$i++;
					?>
                    <tr>
                    	<td><?php echo $i;?></td>
                        <td><?php echo $students['studid'];?></td>
                        <td><?php echo $students['crn'];?> - <?php echo $crn[$students['crn']];?></td>
                    </tr>
                    <?php
				}
				}
				else{
					?>
                    <tr>
                    	<td colspan="3">No result found</td>
                    </tr>
                    <?php
				}
				?>
                </table>
        </div> 
    </div> 

</div>
<?php
include 'footer.php';
?>
