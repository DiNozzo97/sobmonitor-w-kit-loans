<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('login_checker.php');
extract($_GET);
//sob_observations

$stud_obj = $db->query("SELECT * FROM `students` WHERE `student_number` = '$student_no'");
$stud_details = $stud_obj->row;
$student_id = $stud_details['student_id'];
?>
<table width="100%" border="0" cellspacing="0" cellpadding="5" class="grayout_panel">
 <tr>
    <th height="26" colspan="2">VIEW STUDENT NOTES</th>
  </tr>
</table>
<div style="max-height:<?php $height-200;?>px; overflow:auto; padding:10px;">
<h4><?php echo $stud_details['firstname'];?> <?php echo $stud_details['lastname'];?></h4>


<table width="100%" border="0" cellspacing="0" cellpadding="5" class="dues_table" id="reply_table">
    <tr class="dues_header_tr">
        <th width="120">Added by</th>
        <th>Notes</th>
        <th width="100">Date &amp; Time</th>
    </tr>
    <?php
    
	$thread_obj = $db->query("SELECT * FROM `students_note` WHERE `student_id` = '$student_id'");
	$reply_no = $thread_obj->num_rows; 
	if($reply_no>0){
		$replies = $thread_obj->rows;
		$r=0;
		foreach($replies as $reply){
			$r++;
			
			
				 $staff_id = $reply['staff_id'];
				 $staff_obj = $db->query("SELECT * FROM `staffs` WHERE `staff_id` = '$staff_id'");
				 $staff_detail = $staff_obj->row;
				 $staff_name = $staff_detail['firstname'] . ' ' . $staff_detail['lastname'];
			
			?>
			<tr>
				<td><?php echo $staff_name;?></td>
				<td><?php echo $reply['note_text'];?></td>
				<td><?php echo date('d.m.Y H:i',strtotime($reply['note_datetime']));?></td>
			</tr>
			<?php
		}
	}
	else{
		echo '<tr><td colspan="3">No notes found</td></tr>';
	}
    ?>
    </table>
    <br>
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
    <td>
    <form name="reply_form" id="reply_form" enctype="multipart/form-data" method="POST" target="hidden_iframe">
    <input name="student_id" id="student_id" type="hidden" value="<?php echo $student_id;?>">
    <textarea id="note_text" name="note_text" placeholder="Note" style="height:50px; width:98%;"></textarea>
    </form>
    </td>
    <td width="100"><a class="small themebutton button" style="float:right;" onClick="insert_student_note()" href="javascript:;">Submit</a></td>
    </tr>
</table>
</div>