// JavaScript Document



function add_sob(){
	var flag=0;
	
	if(document.getElementById('sob').value==""){
		document.getElementById('sob').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('sob').style.borderColor='';
	}
	
	if(document.getElementById('level_id').value==""){
		document.getElementById('level_id').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('level_id').style.borderColor='';
	}
	
	if(document.getElementById('topic_id').value==""){
		document.getElementById('topic_id').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('topic_id').style.borderColor='';
	}
	
	if(document.getElementById('expected_start_date').value==""){
		document.getElementById('expected_start_date').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('expected_start_date').style.borderColor='';
	}
	
	if(document.getElementById('expected_completion_date').value==""){
		document.getElementById('expected_completion_date').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('expected_completion_date').style.borderColor='';
	}
	
	$('input[name$="keywords[]"]').each(function() {
	
	if ($('input[name$="keywords[]"][value="' + $(this).val() + '"]').size() > 1)
	{
	   $(this).parent().css('border-color','red');
	   flag=1;
	}else
	{
		$(this).parent().css('border-color','');
	}
	});
	
	
	if(flag==0){
		document.sob_form.submit();
		//document.sob_form.reset();
	}
	else{
		$.jGrowl("Please check the highlighted fields");
	}
}

function update_sob(){
	var flag=0;
	
	if(document.getElementById('sob').value==""){
		document.getElementById('sob').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('sob').style.borderColor='';
	}
	
	if(document.getElementById('level_id').value==""){
		document.getElementById('level_id').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('level_id').style.borderColor='';
	}
	
	if(document.getElementById('topic_id').value==""){
		document.getElementById('topic_id').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('topic_id').style.borderColor='';
	}
	
	if(document.getElementById('expected_start_date').value==""){
		document.getElementById('expected_start_date').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('expected_start_date').style.borderColor='';
	}
	
	if(document.getElementById('expected_completion_date').value==""){
		document.getElementById('expected_completion_date').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('expected_completion_date').style.borderColor='';
	}
	
	$('input[name="keywords[]"]').each(function() {
	
	if ($('input[name="keywords[]"][value="' + $(this).val() + '"]').size() > 1)
	{
	   $(this).parent().css('border-color','red');
	   flag=1;
	}else
	{
		$(this).parent().css('border-color','');
	}
	});
	
	
	if(flag==0){
		document.edit_sob_form.submit();
		//document.sob_form.reset();
	}
	else{
		$.jGrowl("Please check the highlighted fields");
	}
}
function after_update_sob_submit(){
	cover_close();
	$.jGrowl("SOB updated successfully");
	/*$('#page_contents').html('Loading... Please wait...').load('list_sobs.php');*/
	search_sob();
}

function after_sob_submit(){
	$.jGrowl("SOB added successfully");
	$('#page_contents').html('Loading... Please wait...').load('list_sobs.php');
}




//function add_sob_keywords(sob_id){
//	var height = $(window).height();
//	var url = "add_sob_keywords.php";
//	grayOut(true,'grayOut_center_div',800);
//	$('#grayOut_center_div').load(url,'height='+height+'&sob_id='+sob_id);
//}

function edit_sob(sob_id){
	var height = $(window).height();
	var url = "edit_sob.php";
	grayOut(true,'grayOut_center_div',800);
	$('#grayOut_center_div').load(url,'height='+height+'&sob_id='+sob_id,function()
	{
			$("#expected_start_date").datepicker({
				onClose: function(selectedDate) {
					$("#expected_completion_date").datepicker("option", "minDate", selectedDate );
				}
			});
			$( "#expected_completion_date").datepicker({
				onClose: function(selectedDate) {
					$("#expected_start_date").datepicker("option", "maxDate", selectedDate );
				}
			});
		$("#sob_keywords").autocomplete("keyword_suggestions.php");
	});
}


function add_new_sob(){
	var height = $(window).height();
	var url = "add_new_sob.php";
	grayOut(true,'grayOut_center_div',800);
	$('#grayOut_center_div').load(url,'height='+height,function(){

			$("#expected_start_date").datepicker({
				onClose: function(selectedDate) {
					$("#expected_completion_date").datepicker("option", "minDate", selectedDate );
				}
			});
			$( "#expected_completion_date").datepicker({
				onClose: function(selectedDate) {
					$("#expected_start_date").datepicker("option", "maxDate", selectedDate );
				}
			});

		
		$("#sob_keywords").autocomplete("keyword_suggestions.php");
	});
}

function delete_sob(sob_id){
	var conf = confirm("Are you sure that you want to delete this sob?");
	if(conf==true){
		$.post('delete_sob.php',{sob_id:sob_id}, function(response){
			$('#hidden_div').html(response);
		})
	}
}

function after_sob_delete(sob_id){
	$('#sob_tr_'+sob_id).remove();
	$.jGrowl("SOB has been deleted");
}

var add_keywords_array =new Array();

function add_keyword(input,event)
{
	var keyCode=event.which? event.which : event.keyCode;

	if(parseInt(keyCode)==13)
	{
		var sob_keywords=$('#sob_keywords').val();
		if(sob_keywords!="")
		{
		var keyword_count=$('#keyword_count').val();
		keyword_count=parseInt(keyword_count)+1
		$('#keyword_count').val(keyword_count);
		
		$('#sob_keywords_tags').append('<li id="key_li_'+keyword_count+'"><div class="keyword_tags" id="tag_id_'+keyword_count+'">'+sob_keywords+'<input class="keywords" type="hidden" name="keywords[]" id="keywords_'+keyword_count+'" value="'+sob_keywords+'" ><a href="javascript:;" onclick="remove_add_keyword('+keyword_count+')">x</a></div></li>');
		add_keywords_array.push(sob_keywords);
		$('#sob_keywords').val('');
		
		}
	}
	
}
function add_keyword_filter(input,event)
{
var keyCode=event.which? event.which : event.keyCode;

	if(parseInt(keyCode)==13)
	{
		var sob_keywords=$('#filter_sob_keywords').val();
		if(sob_keywords!="")
		{
			var keyword_count=$('#filter_keyword_count').val();
			keyword_count=parseInt(keyword_count)+1
			$('#filter_keyword_count').val(keyword_count);
			
			
			$('#hidden_div').load('filter_keyword.php','keyword='+sob_keywords+'&keyword_count='+keyword_count,function(response){
				
				//$('#search_sob_keywords_tags').append('<li id="key_li_'+keyword_count+'"><div class="keyword_tags" id="tag_id_'+keyword_count+'">'+sob_keywords+'<input class="keywords" type="hidden" name="filter_keywords[]" id="keywords_'+keyword_count+'" value="'+sob_keywords+'" ><a href="javascript:;" onclick="remove_add_keyword('+keyword_count+')">x</a></div></li>');
				$('#search_sob_keywords_tags').append(response);
				add_keywords_array.push(sob_keywords);
				$('#filter_sob_keywords').val('');
				
			});
		}
	}
	
}
function keyword_check(input,event)
{

	var keyCode=event.which? event.which : event.keyCode;

	if(parseInt(keyCode)==13)
	{
		var val = input.value;
		input.value="";
		val = encodeURIComponent(val);
		if(val!=""){
			var sob = $('#keyword_sob_id').val();
			$('#hidden_div').load('sob_keyword_map.php','sob_id='+sob+'&keyword='+val,function(response){
				var status = $(response).find('#sob_keyword').val();
				if(status=="1"){
					var tag_id = $(response).find('#key_sob_id').val();
					var html = '<div class="keyword_tags" id="tag_id_'+tag_id+'">'+val+' <a href="javascript:;" onclick="remove_keyword('+tag_id+')">x</a></div>'
					$('#sob_keywords_tags').append(html);
				}
				else{
					$.jGrowl("Sorry!!! Keyword was already tagged to this SOB");
				}
			});
		}
	}
}

function remove_add_keyword(tag_id){
	$('#key_li_'+tag_id).remove();
}

function remove_keyword(tag_id){
	$('#hidden_div').load('remove_keyword.php?tag_id='+tag_id,function(){
		$('#tag_id_'+tag_id).remove();
	});
}
function search_sob()
{
	$('#page_contents').load('list_sobs.php',$('#sob_filter_form').serialize(),function(response){
																						
	});
}

function search_sob_multiple_observe()
{
	$('#page_contents').load('list_sobs_observe.php',$('#sob_filter_form').serialize(),function(response){
																						
	});
}