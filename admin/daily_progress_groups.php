<?php
include_once('config.php');
include_once('db_class.php');

$db = new MySQL_Class(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

//DEFAULT SORTING ORDER

$query =  "
-- student has been observed for some sobs but some others are missing
SELECT students.student_id, students.staff_id, students.firstname as fname, students.lastname as lname, students.student_number , students.student_status, students.email, sum(sobs.level_id=1) as threshold,sum(sobs.level_id=2) as typical,sum(sobs.level_id=3) as excellent from students, sobs where (sobs.sob_id not in (select sob_observations.sob_id from sob_observations where sob_observations.student_id = students.student_id))  and sobs.expected_completion_date < CURDATE() group by students.student_id
UNION
-- student has never been observed
SELECT students.student_id, students.staff_id, students.firstname, students.lastname, students.student_number, students.student_status, students.email, sum(sobs.level_id=1) as threshold,sum(sobs.level_id=2) as typical,sum(sobs.level_id=3) as excellent from students, sobs where students.student_id not in (select distinct sob_observations.student_id from sob_observations)  and sobs.expected_completion_date < CURDATE() group by students.student_id 
UNION
-- there are no sobs to be observed for this student (everything done)
SELECT students.student_id, students.staff_id, students.firstname,
students.lastname, students.student_number, students.student_status,
students.email, 0 as threshold, 0 as typical, 0 as excellent from
students, sobs
where
 not exists (select sobs.sob_id from sobs where
sobs.expected_completion_date < CURDATE() and sob_id not in (select
sob_observations.sob_id from sob_observations where
sob_observations.student_id = students.student_id) )
group by students.student_id
ORDER BY $sorting_field_name $sorting_by";

$student_obj = $db->query($query);
$student_no = $student_obj->num_rows;

if($student_no!=0){
	$students = $student_obj->rows;
	$s=0;
	foreach($students as $student){
          $s++;
          echo "Student: ".$student['student_number'];
          echo "N. threshold: ".$student['threshold'];
	  echo "Status: ".$student['student_status'];
	}
}

?>

