<?php
session_start();
include_once('login_checker.php');
include 'header.php';
include_once('db_class.php');
?>
<style>
.tutor_detail{
	border: 1px solid #cccccc;
    clear: both;
    float: left;
    padding: 20px;
    width: auto;
}

.tutor_detail h1{
	margin:0px;
	padding:0px;
	margin-bottom:5px;	
}

.tutor_detail h3{
	margin:0px;
	padding:0px;
	margin-bottom:5px;	
}

.tutor_detail h4{
	margin:0px;
	padding:0px;	
}
</style>
<script>



function insert_reply(){
	var flag = 0;
	if(document.getElementById('thread_reply').value==""){
		document.getElementById('thread_reply').style.borderColor="#FF0000";
		flag=1;
	}
	else{
		document.getElementById('thread_reply').style.borderColor="";
	}
	
	
	
	if(flag==0){
		var vals = $('#reply_form').serialize();
		$.post('insert_reply.php', vals, function(response){
			$('#reply_table').append(response);
			document.getElementById('thread_reply').value=""
		});
	}
}
function show_topics(){
	$('#topics_holder').html('Loading...').load('show_topics.php');
}

function show_replies(thread_id){
	var height = $(window).height();
	var url = "show_replies.php";
	grayOut(true,'grayOut_center_div',800);
	$('#grayOut_center_div').load(url,'height='+height+'&thread_id='+thread_id);
}
</script>
<div id="wrapper">
    <div id="wrapper_content">
      <h1 class="page_title">Contact Students</h1>
        <div id="page_contents">
			<br>
			<br>

             <div id="topics_holder">          
             	<?php include 'show_topics.php';?>
             </div>
      	</div>
  </div>
</div>
<?php
include 'footer.php';
?>