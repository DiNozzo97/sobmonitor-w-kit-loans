<?php
session_start();
include_once('login_checker.php');
extract($_GET);


function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SOB - Student Report</title>
<link href="css/reports.css" rel="stylesheet" type="text/css" />
</head>

<body>

<?php
$student_query = $db->query("SELECT * FROM `students` WHERE staff_id = '$uid' AND `student_status` = 0");
$student_details = $student_query->rows;
foreach($student_details as $student_detail){
$student_no = $student_detail['student_number'];
?>
<div class="print_view_wrapper">
	<?php
	 $student_id = $student_detail['student_id'];
	 $student_number = $student_detail['student_number'];
	?>
	<h1><?php echo $student_detail['firstname'];?> <?php echo $student_detail['lastname'];?> <span style="float:right;">Printed on : <?php echo date('l jS \of F Y h:i:s A');?></span></h1>
    <h4><?php echo $student_detail['student_number'];?></h4>
	<?php if ($student_detail['visa'] != 0) {
		echo "<h4> Student on a visa</h4>";
	}
	if ($student_detail['foundation'] != 0 ) {
		echo "<h4> Previously on Foundation Year.</h4>";
	}
	?>
    <p></p>
			
	<?php
		
		$seven_days_from_now = date('d.m.Y',strtotime("$today_uk_format +7 days"));
		
		$sob_obj = $db->query("SELECT * FROM `sobs` as s, `topics` as t WHERE s.topic_id = t.topic_id ORDER BY s.topic_id ASC");
		$sob_rows = $sob_obj->rows;
		
		$total_sob = 0;
		$total_observed = 0;
		$observed_1 = 0; // Threshold
		$observed_2 = 0; // Typical
		$observed_3 = 0; // Excellent
		
		$total_1 = 0; // Threshold
		$total_2 = 0; // Typical
		$total_3 = 0; // Excellent
		
		$expected = 0;
		$over_due = 0;
		
		$expected_sob_id = "0";
		$overdue_sob_id = "0";
		$topic_array = array();
		
		$sob_array = array();
		
		$topic_id_array = array();
		foreach($sob_rows as $sob_row){
			$total_sob++;
			
			
			$sob_id = $sob_row['sob_id'];
			$topic_id = $sob_row['topic_id'];
			$level_id = $sob_row['level_id'];
			
			if(!in_array($sob_row['topic_id'],$topic_id_array)){
				$topic_array[] = array("topic_id" => $sob_row['topic_id'], "topic_name" => $sob_row['topic'] );
				${'total_'.$topic_id.'_1'}=0;
				${'observed_'.$topic_id.'_1'}=0;
				${'total_'.$topic_id.'_2'}=0;
				${'observed_'.$topic_id.'_2'}=0;
				${'total_'.$topic_id.'_3'}=0;
				${'observed_'.$topic_id.'_3'}=0;
				array_push($topic_id_array, $topic_id);
				
			}
			
			$expected_date = $sob_row['expected_completion_date'];
			$obs_obj = $db->query("SELECT * FROM `sob_observations` WHERE `student_id` = '$student_id' AND `sob_id` = '$sob_id'");
			$obs_no = $obs_obj->num_rows;
			
			${'total_'.$sob_row['level_id']}++;
			${'total_'.$topic_id.'_'.$sob_row['level_id']}++;
			$observed = 0;
						
			if($obs_no>0){
				$observed = 1;
				$total_observed++;
				${'observed_'.$sob_row['level_id']}++;
				${'observed_'.$topic_id.'_'.$sob_row['level_id']}++;
			}
			
			
			$sob_array[$topic_id][$level_id][] = array("sob_id" => $sob_id, "sob_name" => $sob_row['sob'], "sob_completion_date" => $sob_row['expected_completion_date'], "sob_status" => $observed);
			
		}
		
        ?>
        <br />
        <h2>Summary</h2><br>
         <table width="100%" class="content_table" border="0" cellpadding="10" cellspacing="1">
          <tr class="table_heading">
             <th>&nbsp;</th>
             <th colspan="2">Threshold</th>
             <th colspan="2">Typical</th>
             <th colspan="2">Excellent</th>
          </tr>
          <tr class="table_heading">
             <th>&nbsp;</th>
             <th>Observed</th>
             <th>Total</th>
             <th>Observed</th>
             <th>Total</th>
             <th>Observed</th>
             <th>Total</th>
          </tr>
          	 <tr>
                  <td>Overall</td>
                  <td><?php echo $observed_1;?></Td>
                  <td><?php echo $total_1;?></Td>
                  <td><?php echo $observed_2;?></Td>
                  <td><?php echo $total_2;?></Td>
                  <td><?php echo $observed_3;?></Td>
                  <td><?php echo $total_3;?></Td>
                  
              </tr>
              <?php
			  foreach($topic_array as $topics){
				  $topic_id = $topics['topic_id'];
				  $topic_name = $topics['topic_name'];
				  ?>
                  <tr>
                  <td><?php echo $topic_name;?></td>
                  <td><?php echo ${'observed_'.$topic_id.'_1'};?></Td>
                  <td><?php echo ${'total_'.$topic_id.'_1'};?></Td>
                  <td><?php echo ${'observed_'.$topic_id.'_2'};?></Td>
                  <td><?php echo ${'total_'.$topic_id.'_2'};?></Td>
                  <td><?php echo ${'observed_'.$topic_id.'_3'};?></Td>
                  <td><?php echo ${'total_'.$topic_id.'_3'};?></Td>
              	  </tr>
                  <?php
			  }
			  ?>
		</table>
		<br />

<!--
		<h2>Details</h2><br>
        
        	<?php
			  foreach($topic_array as $topics){
				  $topic_id = $topics['topic_id'];
				  $topic_name = $topics['topic_name'];
				  ?><br />
			
                  <h2><?php echo ucwords($topic_name);?></h2><br>
                  <table width="100%" class="content_table" border="0" cellpadding="10" cellspacing="1">
                  <tr class="table_heading">
                     <th width="60">SOB ID</th>
                     <th>SOB</th>
                     <th width="180">Expected completion date</th>
                  </tr>
                  <tr>
                  	<td colspan="3"><strong>Threshold</strong></td>
                  </tr>
                  <?php
				  $sobs = $sob_array[$topic_id][1];
				  $sobs = array_orderby($sobs, 'sob_status', SORT_DESC);
				  foreach($sobs as $sob){
				  ?>
                  <tr class="<?php if($sob['sob_status']=="1") echo 'completed';?>">
                  <td><?php echo $sob['sob_id'];?></td>
                  <td><?php echo $sob['sob_name'];?></Td>
                  <td><?php echo date_ft($sob['sob_completion_date']);?></Td>
              	  </tr>
                  <?php
				  }
				  ?>
                  <tr>
                  	<td colspan="3"><strong>Typical</strong></td>
                  </tr>
                  <?php
				  $sobs = $sob_array[$topic_id][2];
				  $sobs = array_orderby($sobs, 'sob_status', SORT_DESC);
				  foreach($sobs as $sob){
				  ?>
                  <tr class="<?php if($sob['sob_status']=="1") echo 'completed';?>">
                  <td><?php echo $sob['sob_id'];?></td>
                  <td><?php echo $sob['sob_name'];?></Td>
                  <td><?php echo date_ft($sob['sob_completion_date']);?></Td>
              	  </tr>
                  <?php
				  }
				  ?>
                  
                  <tr>
                  	<td colspan="3"><strong>Excellent</strong></td>
                  </tr>
                  <?php
				  $sobs = $sob_array[$topic_id][3];
				  $sobs = array_orderby($sobs, 'sob_status', SORT_DESC);
				  foreach($sobs as $sob){
				  ?>
                  <tr class="<?php if($sob['sob_status']=="1") echo 'completed';?>">
                  <td><?php echo $sob['sob_id'];?></td>
                  <td><?php echo $sob['sob_name'];?></Td>
                  <td><?php echo date_ft($sob['sob_completion_date']);?></Td>
              	  </tr>
                  <?php
				  }
				  ?>
                  </table>
                  <?php
			  }
			  ?>
        <br />
-->
<br />
      
<?php

	$from = 1;
	$to = 24;


?>
<h1 class="page_title">Timetabled sessions</h1>
 <div style="width:100%; padding:10px 0px 5px 0px;" align="right" id="total_perc">Calculating</div>
 <table width="100%" border="0" cellspacing="1" cellpadding="10" class="content_table">
      <tr class="table_heading">
        <th width="60">CRN</th>
        <th width="230">Details</th>

        <?php
		$total_weeks=0;
		for($i=$from;$i<=$to;$i++){
			?>
            <th>W<?php echo $i;$total_weeks++;?></th>
            <?php
		}
		?>
        <th>% </th>
      </tr>
      <?php
	  $query = $db->query("SELECT c.crn, c.codetype, c.day, c.room, c.starttime, c.endtime, count(s.crn) as expected FROM `CRNlist` as c, `student_timetable` as s WHERE c.crn = s.crn AND s.student_number = '$student_number' group by c.crn");

	  $timetabled_crns = "0";	  
	  
	  if($query->num_rows>0){
	  
	  $crn_list = $query->rows;

	  foreach($crn_list as $crn){
		  $crn_no = $crn['crn'];
		  ?>
      <tr>
        <td><?php echo $crn['crn'];?></td>
        <td><?php echo $crn['codetype'];?> - <?php echo $crn['room'];?> - <?php echo $crn['day'];?> (<?php echo $crn['starttime'];?> - <?php echo $crn['endtime'];?>)</td>

        <?php
	
		$attendees_obj = $db->query("SELECT w.week_number, count(a.week) as attendees FROM `CRNlist` as c, `attendance` as a, `week` as w WHERE  c.crn = a.crn and c.crn = '$crn_no' and w.week_number = a.week and w.week_number between $from and $to AND a.studid = '$student_number' group by w.week_number  ORDER BY w.week_number ASC");
		$attendees_arr = $attendees_obj->rows;
		$attendee = array();
		
		$attended_weeks=0;
		
		foreach($attendees_arr as $attendees ){
			$the_week = $attendees['week_number'];
			$attendee[$the_week] = $attendees['attendees'];
	
		}
		$timetabled_crns.=",".$crn_no;
		
		for($j=$from;$j<=$to;$j++){
			if($attendee[$j]=="1"){
				$present_absent = 'Y';
				$attended_weeks++;
				
			}
			else{
				$present_absent = '-';
			}
			
			
			?>
            <td align="center"><?php echo $present_absent;?></td>
            <?php
		}
		?>
        <td align="center"><?php $perc = ($attended_weeks/$total_weeks)*100; echo number_format($perc,2)?>%</td>
      </tr>
      	<?php
	  }
	  }
	  else{
		 ?>
         <tr>
         	<td colspan="<?php echo $total_weeks+3;?>" align="center"><strong>No timetabled sessions found</strong></td>
         </tr>
         <?php 
	  }
	  ?>
    </table>
    <br />
    
    
    <?php
	
	$query = $db->query("SELECT w.week_id, c.crn, c.codetype, c.room, c.day, c.starttime, c.endtime FROM `CRNlist` as c, `attendance` as a, `week` as w WHERE c.crn = a.crn AND a.studid = '$student_number' AND w.week_id = a.week AND a.crn NOT IN ($timetabled_crns) AND w.week_number between $from and $to");
	
	$all_attendance = $query->rows;
	$num_rows = $query->num_rows;
	//print_r($all_attendance);
	
	if($num_rows>0){
	?>
    <h1 class="page_title">Non-timetabled sessions</h1>
    <?php
	}
	$week_head = 0;
	for($weekrange = $from;$weekrange<=$to;$weekrange++){
		
		for($i=0;$i<$num_rows;$i++){
			if($all_attendance[$i]['week_id'] == $weekrange){
			if($week_head!=$weekrange){
				$week_head=$weekrange;
				
				if($week_head!=0){
					echo '</table><br>';
				}
				?>
                <table width="300" border="0" cellspacing="1" cellpadding="10" class="content_table">
                <tr class="table_heading">
                <th colspan="2"><b>Week <?php echo $week_head ;?></b></th>
                </tr>
				<?php
			}
			?>
			 <tr>
                <td><?php echo $all_attendance[$i]['crn'] ;?></td>
                <td><?php echo $all_attendance[$i]['codetype'];?> - <?php echo $all_attendance[$i]['room'];?> - <?php echo $all_attendance[$i]['day'];?> (<?php echo $all_attendance[$i]['starttime'];?> - <?php echo $all_attendance[$i]['endtime'];?>)</td>
                </tr>
				<?php
			}
		}
		
	}
	
/*	foreach($all_attendance as $attendance){
		?>
        <tr>
        	<td><?php echo $attendance['week_id'];?></td>
            <td><?php echo $attendance['crn'];?></td>
            <td><?php echo $attendance['codetype'];?> - <?php echo $attendance['room'];?> - <?php echo $attendance['day'];?> (<?php echo $attendance['starttime'];?> - <?php echo $attendance['endtime'];?>)</td>
        </tr>
        <?php
	}*/
    ?>
    </table>
    <h1 class="page_title">Notes for this student</h1>

<?php
	$thread_obj = $db->query("SELECT * FROM `students_note` WHERE `student_id` = '$student_id'");
	$reply_no = $thread_obj->num_rows; 
	if($reply_no>0){
?>
	<table width="100%" class="content_table" border="0" cellpadding="10" cellspacing="1">
          <tr class="table_heading">
             <th>Staff name</th>
             <th>Note</th>
             <th>Date</th>
          </tr>
<?php
		$replies = $thread_obj->rows;
		$r=0;
		foreach($replies as $reply){
			$r++;
			
			
				 $staff_id = $reply['staff_id'];
				 $staff_obj = $db->query("SELECT * FROM `staffs` WHERE `staff_id` = '$staff_id'");
				 $staff_detail = $staff_obj->row;
				 $staff_name = $staff_detail['firstname'] . ' ' . $staff_detail['lastname'];
			
			?>
			<tr>
				<td><?php echo $staff_name;?></td>
				<td><?php echo $reply['note_text'];?></td>
				<td><?php echo date('d.m.Y H:i',strtotime($reply['note_datetime']));?></td>
			</tr>
			<?php
		}
		echo '</table>';
	}
	else{
		echo 'No notes found';
	}
?>
<script>
document.getElementById('total_perc').innerHTML='%  - Attendance(100% = <?php echo $total_weeks;?> Weeks)';
</script>

</div>
<br />
<hr />
<br />
<?php
}
?>

</body>
</html>