<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('login_checker.php');
extract($_GET);
//sob_observations



$sob_obj = $db->query("SELECT s.sob_id, s.sob, s.url, s.level_id, s.topic_id, l.level, t.topic, s.expected_completion_date, s.expected_start_date FROM `sobs` s, `levels` l, `topics` t WHERE s.level_id = l.level_id and s.topic_id = t.topic_id AND `sob_id` = '$sob_id'");
$sob = $sob_obj->row;
	

?>
<table width="100%" border="0" cellspacing="0" cellpadding="5" class="grayout_panel">
 <tr>
    <th height="26" colspan="2">Edit SOB</th>
  </tr>
</table>
<form name="edit_sob_form" id="edit_sob_form" enctype="multipart/form-data" action="update_sob_submit.php" method="POST" target="hidden_iframe">
<div style="max-height:<?php $height-200;?>px; overflow:auto; padding:10px;">
<input type="hidden" id="sob_id" name="sob_id" value="<?php echo $sob_id;?>" />
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>

    <td valign="top"><div class="legend_level"><?php echo $sob['level'];?> >> </div> <div class="legend_topic"><?php echo $sob['topic'];?> >> </div> <div class="legend_sob"><?php echo $sob['sob'];?></div></td>
  </tr>
</table>
<br>


<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td width="80"><strong>SOB</strong></td>
<td colspan="3"><textarea id="sob" name="sob" placeholder="Enter SOB" style="height:27px; width:98%;"><?php echo $sob['sob']; ?></textarea>
<input type="hidden" name="sob_source" id="sob_source" value="<?php echo $sob['sob']; ?>">
</td>
</tr>
  <tr>
    <td><strong>URL</strong></td>
    <td colspan="3"><input type="text" id="url" name="url" style="width:98%;" value="<?php echo $sob['url']; ?>" /></td>
  </tr>
  <tr>
    <td width="100"><strong>Notes</strong></td>
    <td colspan="3"><input type="text" id="notes" name="notes" style="width:98%;" value="<?php echo $sob['sob_notes']; ?>" /></td>
  </tr>
<tr>
    <td><strong>Level </strong></td>
    <td><select id="level_id" name="level_id" style="width:250px;">
    	<option value="">Select Level</option>
        <?php
		$levels_obj = $db->query("select * from `levels` where 1");
		$levels = $levels_obj->rows;
		
		foreach($levels as $level){
			?><option <?php if($sob['level_id']==$level['level_id']){ ?> selected <?php } ?> value="<?php echo $level['level_id'];?>"><?php echo $level['level'];?></option><?php
		}
		?>
    </select>&nbsp;&nbsp;</td>
    <td><strong>Topic</strong></td>
    <td><select id="topic_id" name="topic_id" style="width:250px;">
      <option value="">Select Topic</option>
      <?php
		$topics_obj = $db->query("select * from `topics` where 1");
		$topics = $topics_obj->rows;
		
		foreach($topics as $topic){
			?>
      <option <?php if($sob['topic_id']==$topic['topic_id']){ ?> selected <?php } ?>  value="<?php echo $topic['topic_id'];?>"><?php echo $topic['topic'];?></option>
      <?php
		}
		?>
    </select></td>
  </tr>
 <tr>
    <td><strong>Start Dates</strong></td>
    <td><input type="text" id="expected_start_date" style="width:237px;" name="expected_start_date" class="datepicker" value="<?php echo mysql_to_indian_date($sob['expected_start_date']); ?>" placeholder="Start Date" /> &nbsp;&nbsp;</td>
    <td width="150"><strong>Expected Completion Date</strong></td>
    <td><input type="text" id="expected_completion_date" style="width:237px;" name="expected_completion_date" value="<?php echo mysql_to_indian_date($sob['expected_completion_date']); ?>" class="datepicker" placeholder="Expected Completion" /></td>
  </tr>
  <tr>
    <td><strong>Keywords</strong></td>
    <td colspan="3"><input type="text" id="sob_keywords" name="sob_keywords"  onkeyup="add_keyword(this,event)" style="width:496px;" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td height="50" colspan="3">
    <input type="hidden" name="keyword_count" id="keyword_count" value="0">
    <ul id="sob_keywords_tags" style="list-style: none outside none; margin-left: 0; margin-top: -12px; padding: 0; width: 95%;">
    <?php
		$keywords_obj = $db->query("SELECT t.tag_id, k.keyword FROM `keywords` k, `keywords_sobs` t WHERE t.keyword_id = k.keyword_id and t.sob_id = '$sob_id'");
		$key_no = $keywords_obj->num_rows;
		if($key_no>0){
			$keywords = $keywords_obj->rows;
			$keyword_count=0;
			foreach($keywords as $keyword){
				$keyword_count++;
				?>
              
                <li id="key_li_<?php echo $keyword_count; ?>"><div class="keyword_tags" id="tag_id_<?php echo $keyword_count; ?>"><?php echo $keyword['keyword'];?><input class="keywords" type="hidden" name="keywords[]" id="keywords_<?php echo $keyword_count; ?>" value="<?php echo $keyword['keyword'];?>" ><a href="javascript:;" onclick="remove_add_keyword(<?php echo $keyword_count; ?>)">x</a></div></li>
                <?php
			}
		}
		?>
    </ul>
    
    </td>
  </tr>
</table>
<a class="small themebutton button" style="float:right;" onClick="update_sob()" href="javascript:;">Update SOB</a>


</div>
</form>
<script>
$('#keyword_count').val(<?php echo $keyword_count; ?>);
</script>